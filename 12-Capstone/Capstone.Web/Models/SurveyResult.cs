﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models
{
    public class SurveyResult
    {
        public string ParkCode { get; set; }
        public int Results { get; set; }
        public IList<Park> AllParks { get; set; }
        public string ParkName { get; set; }
    }
}
