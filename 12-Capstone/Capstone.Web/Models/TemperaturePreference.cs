﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models
{
    public class TemperaturePreference
    {
        [Display(Name = "Temperature Preference (C) or (F): ")]
        public string WantsCelcius { get; set; }
    }
}
