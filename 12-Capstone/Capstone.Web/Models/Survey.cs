﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models
{
    public class Survey
    {
        public int ID { get; set; }

        [Required]
        public string ParkCode { get; set; }

        [Required]
        [EmailAddress]
        [Display (Name = "Email Address: ")]
        public string EmailAddress { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string ActivityLevel { get; set; }
        public int ?NumberOfResults {get; set;}
    }
}
