﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models
{
    public class SurveySelectionVM
    {
        public IList<SelectListItem> Parks { get; set; }
        public IList<SelectListItem> States { get; set; }
        public IList<SelectListItem> ActivityLevel { get; set; }
        public IList<Park> AllParks { get; set; }
        public Survey Survey { get; set; }
        
    }
}
