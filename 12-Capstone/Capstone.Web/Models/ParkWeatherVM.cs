﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models
{
    public class ParkWeatherVM
    {
        public IList<DailyWeather> Weather { get; set; }
        public Park Park { get; set; }
        public TemperaturePreference TemperaturePreference { get; set; }
    }
}
