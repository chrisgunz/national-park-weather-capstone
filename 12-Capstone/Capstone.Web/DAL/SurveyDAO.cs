﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Web.Controllers;
using Capstone.Web.Models;

namespace Capstone.Web.DAL
{
    public class SurveyDAO : ISurveyDAO
    {
        private readonly string connectionString;

        public SurveyDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }
        // Gets a list of top parks by survey
        public IList<SurveyResult> GetSurveys()
        {
            IList <SurveyResult> surveyList = new List<SurveyResult>();
            try
            {

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string sql = "select max(survey_result.parkCode) as parkCode, Count(survey_result.surveyId) as results from survey_result join park on park.parkCode = survey_result.parkCode group by survey_result.parkCode order by results desc, parkCode asc;";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while(reader.Read())
                    {
                        SurveyResult survey = new SurveyResult();
                        // Convert the park code to the park name for display
                        survey.ParkCode = Convert.ToString(reader["parkCode"]);
                        survey.Results = Convert.ToInt32(reader["results"]);
                        surveyList.Add(survey);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return surveyList; 
        }
        
        // Allows the user to create a new survey
        public void CreateSurvey(Survey survey)
        {
            try
            {
                
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string sql = "INSERT INTO survey_result (parkCode, emailAddress, state, activityLevel)" +
                                                    "VALUES (@parkCode, @emailAddress, @state, @activityLevel);";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@parkCode", survey.ParkCode);
                    cmd.Parameters.AddWithValue("@emailAddress", survey.EmailAddress);
                    cmd.Parameters.AddWithValue("@state", survey.State);
                    cmd.Parameters.AddWithValue("@activityLevel", survey.ActivityLevel);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        // Returns a single park by code
        public string GetParkByCode(string code)
        {
            Park park = new Park();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM park WHERE parkCode = @parkCode", conn);
                    cmd.Parameters.AddWithValue("@parkCode", code);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        park = RowToPark(reader);
                    }

                }
                return park.Name;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        private Park RowToPark(SqlDataReader row)
        {
            string imageType = ".jpg";
            Park park = new Park();
            park.Code = Convert.ToString(row["parkCode"]);
            park.Name = Convert.ToString(row["parkName"]);
            park.State = Convert.ToString(row["state"]);
            park.Acreage = Convert.ToInt32(row["acreage"]);
            park.Elevation = Convert.ToInt32(row["elevationInFeet"]);
            park.MilesOfTrail = Convert.ToInt32(row["milesOfTrail"]);
            park.NumberOfCampsites = Convert.ToInt32(row["numberOfCampsites"]);
            park.Climate = Convert.ToString(row["climate"]);
            park.YearFounded = Convert.ToInt32(row["yearFounded"]);
            park.AnnualVisitorCount = Convert.ToInt32(row["annualVisitorCount"]);
            park.Quote = Convert.ToString(row["inspirationalQuote"]);
            park.QuoteSource = Convert.ToString(row["inspirationalQuoteSource"]);
            park.Description = Convert.ToString(row["parkDescription"]);
            park.EntryFee = Convert.ToDecimal(row["entryFee"]);
            park.NumberOfSpecies = Convert.ToInt32(row["numberOfAnimalSpecies"]);
            park.ImageSource = park.Code + imageType;
            return park;
        }
    }
}
