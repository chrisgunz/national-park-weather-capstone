﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAL
{
    public class WeatherDAO : IWeatherDAO
    {
        private readonly string connectionString;

        public WeatherDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        // Maps rows from the database to our park object
        private DailyWeather RowToWeather(SqlDataReader row)
        {
            DailyWeather weather = new DailyWeather();
            weather.ParkCode = Convert.ToString(row["parkCode"]);
            weather.Forecast = Convert.ToString(row["forecast"]);
            weather.Day = Convert.ToInt32(row["fiveDayForecastValue"]);
            weather.Low = Convert.ToInt32(row["low"]);
            weather.High = Convert.ToInt32(row["high"]);
            weather.ImageSource = weather.Forecast.Replace(" ", "") + ".png";
            return weather;
        }

        // Gets a parks weather forecast by it's code
        public IList<DailyWeather> GetWeatherByCode(string code, string wantsCelcius)
        {
            IList<DailyWeather> weather = new List<DailyWeather>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM weather WHERE parkCode = @parkCode", conn);
                    cmd.Parameters.AddWithValue("@parkCode", code);

                    SqlDataReader reader = cmd.ExecuteReader();                    
                    if (wantsCelcius.ToLower() == "c")
                    {
                        while (reader.Read())
                        {
                            DailyWeather celciusWeather = new DailyWeather();
                            celciusWeather.ParkCode = Convert.ToString(reader["parkCode"]);
                            celciusWeather.Forecast = Convert.ToString(reader["forecast"]);
                            celciusWeather.Day = Convert.ToInt32(reader["fiveDayForecastValue"]);
                            celciusWeather.Low = Convert.ToInt32(reader["low"]);
                            celciusWeather.High = Convert.ToInt32(reader["high"]);
                            celciusWeather.ImageSource = celciusWeather.Forecast.Replace(" ", "") + ".png";
                            celciusWeather.Low = (celciusWeather.Low - 32) * 5 / 9;
                            celciusWeather.High = (celciusWeather.High - 32) * 5 / 9;
                            weather.Add(celciusWeather);
                        }
                    }
                    else if (wantsCelcius.ToLower() == "f")
                    {
                        while (reader.Read())
                        {
                            weather.Add(RowToWeather(reader));
                        }
                    }
                    else
                    {
                        while (reader.Read())
                        {
                            weather.Add(RowToWeather(reader));
                        }
                    }
                }
                return weather;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
