﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAL
{
    public interface ISurveyDAO
    {
        // Returns a list of all surveys
        IList<SurveyResult> GetSurveys();

        // Allows the user to create a new survey
        void CreateSurvey(Survey survey);

        // Gets a park code from a park name
        //string GetParkCodeByName(string parkName);
    }
}
