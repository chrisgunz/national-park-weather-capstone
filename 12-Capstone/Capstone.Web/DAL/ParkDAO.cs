﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAL
{
    public class ParkDAO : IParkDAO
    {
        private readonly string connectionString;

        public ParkDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        // Returns a list of all parks
        public IList<Park> GetParks()
        {
            IList<Park> parkList = new List<Park>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM park", conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        parkList.Add(RowToPark(reader));
                    }
                }
                return parkList;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        // Maps rows from the database to our park object
        private Park RowToPark(SqlDataReader row)
        {
            string imageType = ".jpg";
            Park park = new Park();
            park.Code = Convert.ToString(row["parkCode"]);
            park.Name = Convert.ToString(row["parkName"]);
            park.State = Convert.ToString(row["state"]);
            park.Acreage = Convert.ToInt32(row["acreage"]);
            park.Elevation = Convert.ToInt32(row["elevationInFeet"]);
            park.MilesOfTrail = Convert.ToInt32(row["milesOfTrail"]);
            park.NumberOfCampsites = Convert.ToInt32(row["numberOfCampsites"]);
            park.Climate = Convert.ToString(row["climate"]);
            park.YearFounded = Convert.ToInt32(row["yearFounded"]);
            park.AnnualVisitorCount = Convert.ToInt32(row["annualVisitorCount"]);
            park.Quote = Convert.ToString(row["inspirationalQuote"]);
            park.QuoteSource = Convert.ToString(row["inspirationalQuoteSource"]);
            park.Description = Convert.ToString(row["parkDescription"]);
            park.EntryFee = Convert.ToDecimal(row["entryFee"]);
            park.NumberOfSpecies = Convert.ToInt32(row["numberOfAnimalSpecies"]);
            park.ImageSource = park.Code + imageType;
            return park;
        }

        // Returns a single park by code
        public Park GetParkByCode(string code)
        {
            Park park = new Park();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM park WHERE parkCode = @parkCode", conn);
                    cmd.Parameters.AddWithValue("@parkCode", code);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        park = RowToPark(reader);
                    }

                }
                return park;
            }            
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public string GetParkNameFromCode(string code)
        {
            Park park = new Park();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM park WHERE parkCode = @parkCode", conn);
                    cmd.Parameters.AddWithValue("@parkCode", code);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        park = RowToPark(reader);
                    }

                }
                return park.Name;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

    }
}
