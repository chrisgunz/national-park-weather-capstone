﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAL
{
    public interface IWeatherDAO
    {
        // Gets the forecast for a park using it's code
        IList<DailyWeather> GetWeatherByCode(string code, string wantsCelcius);
    }
}
