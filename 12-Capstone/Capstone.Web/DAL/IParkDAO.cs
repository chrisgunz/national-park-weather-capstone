﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAL
{
    public interface IParkDAO
    {
        /// <summary>
        /// Returns a full list of all parks
        /// </summary>
        /// <returns></returns>
        IList<Park> GetParks();

        /// <summary>
        /// Returns a single park dictated by Id
        /// </summary>
        /// <returns></returns>
        Park GetParkByCode(string code);

        /// <summary>
        /// Returns the name of a park, derived from it's code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        string GetParkNameFromCode(string code);
    }
}
