﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Capstone.Web.Models;
using Capstone.Web.DAL;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Capstone.Web.Controllers
{
    public class HomeController : Controller
    {
        private IParkDAO parkDAO = null;
        private IWeatherDAO weatherDAO = null;

        public HomeController(IParkDAO parkDAO, IWeatherDAO weatherDAO)
        {
            this.parkDAO = parkDAO;
            this.weatherDAO = weatherDAO;
        }
        public IActionResult Index()
        {
            IList<Park> parkList = parkDAO.GetParks();
            return View(parkList);
        }
        [HttpGet]
        public IActionResult Detail(string Code)
        {
            string pref = GetPreferredTemp();
            if (Code == null)
            {
                return NotFound();
            }            
            ParkWeatherVM vm = new ParkWeatherVM();
            vm.Park = parkDAO.GetParkByCode(Code);
            vm.Weather = weatherDAO.GetWeatherByCode(Code, pref);
            return View(vm);
        }

        [HttpGet]
        public IActionResult Preferences()
        {
            TemperaturePreference pref = new TemperaturePreference()
            {
                WantsCelcius = GetPreferredTemp()
            };
            return View(pref);
        }

        private string GetPreferredTemp()
        {
            return HttpContext.Session.GetString("PreferredTemp") ?? "";
        }

        [HttpPost]
        public IActionResult Preferences(TemperaturePreference pref)
        {
            SetPreferredTemp(pref.WantsCelcius);
            return RedirectToAction("Index", "Home");
        }

        private void SetPreferredTemp(string temp)
        {
            if (temp == null || temp.Trim().Length == 0)
            {
                ClearPreferredTemp();
            }
            else
            {
                HttpContext.Session.SetString("PreferredTemp", temp);
            }
        }

        private void ClearPreferredTemp()
        {
            HttpContext.Session.Remove("PreferredTemp");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }



        //// Select list for the temp selection
        //public IList<SelectListItem> activityLevels = new List<SelectListItem>()
        //{
        //    new SelectListItem() {Text = "Celcius", Value = "C"},
        //    new SelectListItem() {Text = "Farenheight", Value = "F"},
        //};
    }
}
